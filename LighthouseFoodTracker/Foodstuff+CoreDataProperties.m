//
//  Foodstuff+CoreDataProperties.m
//  LighthouseFoodTracker
//
//  Created by James Cash on 03-02-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff+CoreDataProperties.h"

@implementation Foodstuff (CoreDataProperties)

@dynamic calories;
@dynamic name;
@dynamic eaten;

@end
