//
//  Foodstuff+CoreDataProperties.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 03-02-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff.h"

NS_ASSUME_NONNULL_BEGIN

@interface Foodstuff (CoreDataProperties)

@property (nonatomic) double calories;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *eaten;

@end

@interface Foodstuff (CoreDataGeneratedAccessors)

- (void)addEatenObject:(NSManagedObject *)value;
- (void)removeEatenObject:(NSManagedObject *)value;
- (void)addEaten:(NSSet<NSManagedObject *> *)values;
- (void)removeEaten:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
