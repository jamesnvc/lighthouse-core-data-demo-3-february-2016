//
//  ViewController.m
//  LighthouseFoodTracker
//
//  Created by James Cash on 03-02-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Foodstuff.h"

@interface ViewController () <UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *foodNameField;
@property (weak, nonatomic) IBOutlet UITextField *caloriesField;
@property (weak, nonatomic) IBOutlet UITableView *foodsTable;
@property (strong,nonatomic) NSFetchedResultsController *resultsController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    NSFetchRequest *allFoodsReq = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
    //    allFoodsReq.predicate = [NSPredicate predicateWithFormat:@"name contains %@", @"ham"];
    allFoodsReq.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES] ];

    self.resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:allFoodsReq managedObjectContext:del.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    self.resultsController.delegate = self;
    NSError *err = nil;
    if (![self.resultsController performFetch:&err]) {
        NSLog(@"oh shit %@", err.localizedDescription);
        abort();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveFood:(id)sender {
    NSString *foodName = self.foodNameField.text;
    double foodCalories = self.caloriesField.text.doubleValue;
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    Foodstuff *newFood = [NSEntityDescription insertNewObjectForEntityForName:@"Foodstuff" inManagedObjectContext:del.managedObjectContext];
    newFood.name = foodName;
    newFood.calories = foodCalories;
    [del saveContext];
    self.foodNameField.text = @"";
    self.caloriesField.text = @"";

//    [self.foodsTable reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    AppDelegate *del = [UIApplication sharedApplication].delegate;
//    NSError *err = nil;
//    NSFetchRequest *allFoodsReq = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
////    allFoodsReq.predicate = [NSPredicate predicateWithFormat:@"name contains %@", @"ham"];
//    NSArray *allFoods = [del.managedObjectContext executeFetchRequest:allFoodsReq error:&err];
//    return [allFoods count];

    if ([self.resultsController.sections count] > 0) {
        return [self.resultsController.sections[0] numberOfObjects];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodCell"];
    UILabel *foodName = [cell viewWithTag:1];
    UILabel *foodCalories = [cell viewWithTag:2];

//    AppDelegate *del = [UIApplication sharedApplication].delegate;
//    NSError *err = nil;
//    NSFetchRequest *allFoodsReq = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
//    //    allFoodsReq.predicate = [NSPredicate predicateWithFormat:@"name contains %@", @"ham"];
//    allFoodsReq.sortDescriptors = @[ [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] ];
//    NSArray *allFoods = [del.managedObjectContext executeFetchRequest:allFoodsReq error:&err];
//    Foodstuff *cellFood = allFoods[indexPath.item];

    Foodstuff *cellFood = [self.resultsController objectAtIndexPath:indexPath];

    foodName.text = cellFood.name;
    foodCalories.text = [NSString stringWithFormat:@"%f kcal", cellFood.calories];

    return cell;
}

#pragma mark - nsfetchresultscontrollerdelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.foodsTable beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.foodsTable endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.foodsTable insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case NSFetchedResultsChangeDelete:
            [self.foodsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}

@end
