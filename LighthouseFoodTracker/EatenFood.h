//
//  EatenFood.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 03-02-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Foodstuff;

NS_ASSUME_NONNULL_BEGIN

@interface EatenFood : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "EatenFood+CoreDataProperties.h"
